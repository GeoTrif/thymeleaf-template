package com.template.controller;

import com.template.model.FullStudentDTO;
import com.template.model.Student;
import com.template.model.StudentDTO;
import com.template.model.StudentId;
import com.template.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class StudentController {

    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/")
    public String getStudentList(Model model) {
        model.addAttribute("student", new Student());
        model.addAttribute("id", new StudentId());

        model.addAttribute("students", studentService.getStudents());

        return "student";
    }

    @PostMapping("/student-save")
    public String saveStudentToDb(@ModelAttribute StudentDTO studentDTO, Model model) {
        Student student = new Student();
        student.setFirstName(studentDTO.getFirstName());
        student.setLastName(studentDTO.getLastName());
        student.setAge(studentDTO.getAge());

        studentService.saveStudent(student);
        model.addAttribute("student", student);
        model.addAttribute("id", new StudentId());

        return "student";
    }

    @PostMapping("/student-edit")
    public String editStudentDetails(@ModelAttribute FullStudentDTO fullStudentDTO, Model model) {
        studentService.updateStudent(fullStudentDTO);
        model.addAttribute("student", new Student());
        model.addAttribute("id", new StudentId());

        return "student";
    }

    @PostMapping("/student-delete")
    public String deleteStudentFromDb(@ModelAttribute StudentId id, Model model) {
        studentService.deleteStudent(id.getId());
        model.addAttribute("id", id);
        model.addAttribute("student", new Student());

        return "student";
    }
}
