package com.template.service;

import com.template.model.FullStudentDTO;
import com.template.model.Student;
import com.template.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    private StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudents() {
        List<Student> students = new ArrayList<>();
        studentRepository.findAll().forEach(students::add);

        return students;
    }

    public void saveStudent(Student student) {
        studentRepository.save(student);
    }

    public void deleteStudent(int id) {
        studentRepository.deleteById(id);
    }

    public void updateStudent(FullStudentDTO fullStudentDTO) {
        Student student = studentRepository.findById(fullStudentDTO.getId()).get();
        student.setFirstName(fullStudentDTO.getFirstName());
        student.setLastName(fullStudentDTO.getLastName());
        student.setAge(fullStudentDTO.getAge());

        studentRepository.save(student);
    }
}
